


export default class AdaptUtils {
    public static isLongScreen(): boolean {

        if (!cc.sys.isNative) {
            cc.log("非原生 false");
            return false;
        }

        let frameSize = cc.view.getFrameSize();

        let frameHeight = frameSize.height;
        let frameWidth = frameSize.width;

        let scale = Math.max(frameWidth / frameHeight, frameHeight / frameWidth);  //宽高比

        return scale >= 2;
    }

    public static getLiuhaiWidthPx(): number {
        return 50;
    }
}

