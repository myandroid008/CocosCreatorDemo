import SuperScrollView from "./SuperScrollView";


const { ccclass, property } = cc._decorator;

@ccclass
export default class FrameLoading extends cc.Component {

    @property(SuperScrollView)
    ndSv: SuperScrollView = null;


    dataList: any[] = [];

    start() {
        for (let i = 1; i <= 500; i++) {
            this.dataList.push(i);
        }
    }


    commonLoadingItems() {
        if (!this.ndSv.canInputData()) {
            cc.log("正在加载中")
            return;
        }

        this.ndSv.setData(this.dataList, false, () => {
            this.ndSv.scrollToIndex(300);
        });

    }

    frameLoadingItems() {
        if (!this.ndSv.canInputData()) {
            cc.log("正在加载中")
            return;
        }
        this.ndSv.setData(this.dataList, true, () => {
            this.ndSv.scrollToIndex(300);
        });
    }
}
