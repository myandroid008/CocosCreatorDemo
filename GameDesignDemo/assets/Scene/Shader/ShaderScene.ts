// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class ShaderScene extends cc.Component {

    @property(cc.Node)
    nd2: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start() {
        // this.traverseChildren(this.nd2);
        this.grayChildren(this.nd2);
    }

    traverseChildren(parentNode: cc.Node) {
        let children = parentNode.children;

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            cc.log(child.name);
            this.traverseChildren(child);
        }
    }

    grayChildren(parentNode: cc.Node) {
        let children = parentNode.children;

        for (let i = 0; i < children.length; i++) {
            let child = children[i];
            child.color = new cc.Color(82, 139, 139);
            this.grayChildren(child);
        }
    }



    // update (dt) {}
}
